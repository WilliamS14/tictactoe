import Game from '@/components/Game';

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center p-24">
      <h1>Tic Tac Toe</h1>
      <Game />
    </main>
  );
}
